# README
Created 09/08/2014, Wes W.

## Hang w/ Admin v2

This version of the Hang w/ admin panel is built on of the MEAN (MongoDB, Express, Angualar, Node) stack.

The codebase for this project uses a [Yeoman](http://yeoman.io/) generator, in particular the angular-fullstack generator.
This yo generator can be found on [github](https://github.com/DaftMonk/generator-angular-fullstack#usage).

### Project Structure

+ Gruntfile.js - grunt config file
+ README.md - this file
+ bower.json - list of packages for Bower
+ client - folder containing all front-end (in browser) code
+ e2e - test specs for working with Protractor
+ karam.conf.js - testing
+ node_modules - all installed node packages issued by ```npm install```; this folder has been added to .gitignore
+ package.json - file detailing this project and its packages; node looks at this file to install packages into node_modules
+ protractor.conf.js - protractor test config
+ server - folder containing all backend (server-side) code


### Helpful Tools

This project comes bundled with several useful tools for developing MEAN apps.

+ Jasmine for unit tests
+ Protractor for end-to-end testing
+ Bower for frontend JS library management
+ Grunt automated task runner for JS
+ Twitter Bootstrap 3 for responsive design, grid, and common HTML elements
+ Font Awesome - scalable CSS and Font toolkit 

### Requirements for Installing on Localhost

Need the following to be installed:
+ Node.js and NPM
+ MongoDB with connection to localhost:27017

Steps to Setup:
+ Pull code from BitBucket
+ ```cd``` into project root directory and ```npm install```
+ Type in ```grunt serve``` to launch server on port 9000
