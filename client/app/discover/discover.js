'use strict';

angular.module('hangwAdminV2App')
  .config(function ($stateProvider) {
    $stateProvider
      .state('discover', {
        url: '/discover',
        templateUrl: 'app/discover/discover.html',
        controller: 'DiscoverCtrl'
      });
  });