'use strict';

angular.module('hangwAdminV2App')
    .controller('CategoriesCtrl', function ($scope, $http, categoryService) {

        $scope.title = 'Categories';
        $scope.categories = [];

        $scope.getTotal = function () {
            return $scope.categories.length;
        };
    

        function loadIndex() {
            $scope.categories = categoryService.getCategories();
        }
        
        loadIndex();
    })
      
   //Service singleton for data fetch - refactor out into its own file
   .service('categoryService', function($http, $q) {
    
    var categories = [
        {name: 'one', value: 3, used: true},
        {name: 'alskdjflkaj', value: 32, used: false},
        {name: 'onjflakjfke', value: 5, used: true},
        {name: 'jdflajhflkajlk', value: 73, used: true},
    ];        
            
    this.getCategories = function() {
        return categories;
        
//        return $http({
//            url: 'https://dev-api.hangwith.com/v1/category', 
//            method: 'GET'
//        });
        
//        return $http.get('https://dev-api.hangwith.com/v1/category').success(function(result) {
//            alert(result.data);
//        });
//        
//        console.log('inside getCategories inside categoryService');
//        
//        $http.get('https://dev-api.hangwith.com/v1/category')
//            .then(function(result) {
//                console.log(result.data);
//        });
    }
});
            