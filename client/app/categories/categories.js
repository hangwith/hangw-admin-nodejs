'use strict';

angular.module('hangwAdminV2App')
  .config(function ($stateProvider) {
    $stateProvider
      .state('categories', {
        url: '/categories',
        templateUrl: 'app/categories/categories.html',
        controller: 'CategoriesCtrl',
        authenticate: true
      });
  });